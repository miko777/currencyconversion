//
//  ExchangeRateService.swift
//  CurrencyConversionApp
//
//  Created by Damjan Miko on 13/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ObjectMapper

enum APIParsingError: Error {
    case dataToStringError
}

class ExchangeRateService {
    
    private let sessionService: URLSession
    private let hnbExchangeRateUrl = "http://hnbex.eu/api/v1/rates/daily"
    
    init() {
        sessionService = URLSession.shared
    }
    
    func fetchExchangeRates() -> Observable<[CurrencyRate]>{
        let url = URL(string: hnbExchangeRateUrl)!
        let request = URLRequest.createGetRequest(with: url)
        
        return sessionService.rx
            .response(request: request)
            .map { (response, data) -> [CurrencyRate] in
                guard let stringData = String(data: data, encoding: .utf8),
                    let exchangeRates = Mapper<CurrencyRate>().mapArray(JSONString: stringData)
                    else { throw APIParsingError.dataToStringError }
                return exchangeRates
                
        }
    }
}
