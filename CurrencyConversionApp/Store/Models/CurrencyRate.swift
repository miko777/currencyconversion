//
//  CurrencyRate.swift
//  CurrencyConversionApp
//
//  Created by Damjan Miko on 13/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

import ObjectMapper

class CurrencyRate: Mappable {
    var currencyCode: String!
    var medianRate: String!
    var unitValue: Int!
    var sellingRate: String!
    var buyingRate: String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        currencyCode <- map["currency_code"]
        medianRate <- map["median_rate"]
        unitValue <- map["unit_value"]
        sellingRate <- map["selling_rate"]
        buyingRate <- map["buying_rate"]

    }
    
}
