//
//  AppCoordinator.swift
//  CurrencyConversionApp
//
//  Created by Damjan Miko on 13/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

import UIKit

class AppCoordinator {
    
    private var window: UIWindow!
    private var appDependecies: AppDependecies
    
    init(window: UIWindow) {
        self.window = window
        self.appDependecies = AppDependecies()
        presentInitialScreen()
    }
    
    public func presentInitialScreen() {
        let currencyConvertorViewModel = CurrencyConvertorViewModel(dependecies: appDependecies)
        let currencyConvertorViewController = CurrencyConvertorViewController(viewModel: currencyConvertorViewModel)
        window.rootViewController = UINavigationController(rootViewController: currencyConvertorViewController)
        window.makeKeyAndVisible()
    }
    
}
