//
//  AppDependecies.swift
//  CurrencyConversionApp
//
//  Created by Damjan Miko on 13/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

class AppDependecies {
    var exchangeRateService: ExchangeRateService
    
    init() {
        exchangeRateService = ExchangeRateService()
    }
}
