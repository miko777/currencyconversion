//
//  String+Extensions.swift
//  CurrencyConversionApp
//
//  Created by Damjan Miko on 13/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

import Foundation

extension String {
    var floatValue: Float {
        return (self as NSString).floatValue
    }
}
