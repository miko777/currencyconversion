//
//  URLRequest.swift
//  CurrencyConversionApp
//
//  Created by Damjan Miko on 13/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

import Foundation

extension URLRequest {
    static func createGetRequest(with url: URL) -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        return urlRequest
    }
}
