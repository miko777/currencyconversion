//
//  CurrencyConvertorViewController.swift
//  CurrencyConversionApp
//
//  Created by Damjan Miko on 13/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

import RxSwift
import UIKit

class CurrencyConvertorViewController: UIViewController {
    
    var viewModel: CurrencyConvertorViewModel!
    
    var fromLabel: UILabel!
    var toLabel: UILabel!
    var fromPickerView: UIPickerView!
    var toPickerView: UIPickerView!
    var submitButton: UIButton!
    var resultLabel: UILabel!
    
    var fromCurrency: String!
    var toCurrency: String!
    let horizontalMargin: CGFloat = 10
    
    var currencyRateList = Variable<[CurrencyRate]>([])
    
    var disposeBag = DisposeBag()
    
    convenience init(viewModel: CurrencyConvertorViewModel) {
        self.init()
        self.title = UITexts.currencyConvertorTitle
        self.viewModel = viewModel
    }
    
    override func loadView() {
        buildViews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.exchangeRates.map{ [weak self] currencyRateList in
                self?.fromCurrency = currencyRateList.first?.currencyCode
                self?.toCurrency = currencyRateList.first?.currencyCode
                return currencyRateList
            }
            .bind(to: currencyRateList)
            .disposed(by: disposeBag)
        
        viewModel.currenciesCode
            .bind(to: fromPickerView.rx.itemTitles) { (row, element) in
                return element
            }
            .disposed(by: disposeBag)
        
        fromPickerView.rx.modelSelected(String.self)
            .subscribe(onNext: { [weak self] currency in
                self?.fromCurrency = currency[0]
            })
            .disposed(by: disposeBag)
        
        viewModel.currenciesCode
            .bind(to: toPickerView.rx.itemTitles) { (row, element) in
                return element
            }
            .disposed(by: disposeBag)
        
        toPickerView.rx.modelSelected(String.self)
            .subscribe(onNext: { [weak self] currency in
                self?.toCurrency = currency[0]
            })
            .disposed(by: disposeBag)
        
        submitButton
            .rx.tap
            .subscribe(onNext: { [weak self] _ in
                guard let `self` = self,
                    let fromCurrencyRate = self.currencyRateList.value.filter({$0.currencyCode == self.fromCurrency}).first,
                    let toCurrencyRate = self.currencyRateList.value.filter({$0.currencyCode == self.toCurrency}).first
                    else { return }
                let fromCurrencyBuyingRate = fromCurrencyRate.buyingRate.floatValue
                let toCurrencySellingRate = toCurrencyRate.sellingRate.floatValue
                
                let convertingValue = fromCurrencyBuyingRate / toCurrencySellingRate *
                    Float(toCurrencyRate.unitValue!)
                
                let convertingValueString = String(format: "%.2f", convertingValue)
                if let unitValue = fromCurrencyRate.unitValue,
                    let fromCurrencyCode = fromCurrencyRate.currencyCode,
                    let toCurrencyCode = toCurrencyRate.currencyCode {
                    
                    self.resultLabel.text = "\(unitValue) \(fromCurrencyCode) = \(convertingValueString) \(toCurrencyCode)"
                }
                
                
            }).disposed(by: disposeBag)
    
    }
}

