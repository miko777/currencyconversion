//
//  CurrencyConvertorViewController+Design.swift
//  CurrencyConversionApp
//
//  Created by Damjan Miko on 13/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

import UIKit
import PureLayout

extension CurrencyConvertorViewController {
    
    func buildViews() {
        view = UIView()
        view.backgroundColor = UIColor.white
        
        let titleStackView = createStackView()
        view.addSubview(titleStackView)
        titleStackView.autoPinEdgesToSuperviewEdges(with:
            UIEdgeInsets(top: 76, left: horizontalMargin, bottom: 0, right: horizontalMargin),
                                               excludingEdge: .bottom)
        
        fromLabel = createTitleLabel()
        fromLabel.text = UITexts.from
        titleStackView.addArrangedSubview(fromLabel)
        
        toLabel = createTitleLabel()
        toLabel.text = UITexts.to
        titleStackView.addArrangedSubview(toLabel)
        
        let pickerStackView = createStackView()
        view.addSubview(pickerStackView)
        pickerStackView.autoPinEdge(.top, to: .bottom, of: titleStackView, withOffset: 10)
        pickerStackView.autoPinEdge(toSuperviewEdge: .leading, withInset: horizontalMargin)
        pickerStackView.autoPinEdge(toSuperviewEdge: .trailing, withInset: horizontalMargin)
        
        fromPickerView = UIPickerView()
        pickerStackView.addArrangedSubview(fromPickerView)
        
        toPickerView = UIPickerView()
        pickerStackView.addArrangedSubview(toPickerView)
        
        submitButton = UIButton(type: .system)
        submitButton.setTitleColor(UIColor.black, for: .normal)
        submitButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        submitButton.setTitle(UITexts.submit, for: .normal)
        submitButton.layer.borderColor = UIColor.brown.cgColor
        submitButton.layer.borderWidth = 1.5
        view.addSubview(submitButton)
        submitButton.autoPinEdge(.top, to: .bottom, of: pickerStackView, withOffset: 20)
        submitButton.autoPinEdge(toSuperviewEdge: .leading, withInset: horizontalMargin)
        submitButton.autoPinEdge(toSuperviewEdge: .trailing, withInset: horizontalMargin)
        
        resultLabel = UILabel()
        resultLabel.textAlignment = .center
        resultLabel.textColor = UIColor.black
        resultLabel.font = UIFont.boldSystemFont(ofSize: 25)
        resultLabel.numberOfLines = 0
        view.addSubview(resultLabel)
        resultLabel.autoPinEdge(.top, to: .bottom, of: submitButton, withOffset: 40)
        resultLabel.autoPinEdge(toSuperviewEdge: .leading, withInset: horizontalMargin)
        resultLabel.autoPinEdge(toSuperviewEdge: .trailing, withInset: horizontalMargin)
        resultLabel.autoPinEdge(toSuperviewEdge: .bottom, withInset: 10, relation: .greaterThanOrEqual)
    }
    
    func createTitleLabel() -> UILabel {
        let titleLabel = UILabel()
        titleLabel.textAlignment = .center
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
        return titleLabel
    }
    
    func createStackView() -> UIStackView {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.alignment = .center
        stackView.spacing = 20
        return stackView
    }
    
}
