//
//  CurrencyConvertorViewModel.swift
//  CurrencyConversionApp
//
//  Created by Damjan Miko on 13/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

import RxSwift

class CurrencyConvertorViewModel {
    
    var exchangeRates: Observable<[CurrencyRate]>!
    var currenciesCode: Observable<[String]>!
    
    var disposeBag = DisposeBag()
    
    init(dependecies: AppDependecies) {
        
        exchangeRates = dependecies.exchangeRateService.fetchExchangeRates()
        
        currenciesCode = exchangeRates.map{ exchangeRatesElements -> [String] in
            exchangeRatesElements.map{ exchangeRatesElement -> String in
                exchangeRatesElement.currencyCode
            }
        }
    }
}
