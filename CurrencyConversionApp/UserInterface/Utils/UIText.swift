//
//  UIText.swift
//  CurrencyConversionApp
//
//  Created by Damjan Miko on 13/09/2018.
//  Copyright © 2018 Damjan Miko. All rights reserved.
//

import Foundation

class UITexts: NSObject {
    
    static let from = "From:"
    static let to = "To:"
    static let submit = "Submit"
    static let currencyConvertorTitle = "Currency Convertor"
}
